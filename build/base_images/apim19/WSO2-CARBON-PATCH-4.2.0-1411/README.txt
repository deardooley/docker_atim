Patch ID         : WSO2-CARBON-PATCH-4.2.0-1411
Applies To       : am-1.9.0
Associated JIRA  : https://wso2.org/jira/browse/APIMANAGER-3952


DESCRIPTION
-----------

This patch fix the failing of Token Generation when subscriber is from a ReadOnly Secondary User Store associated with the above public JIRA.




INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch1411 to  <CARBON_SERVER>/repository/components/patches/ directory.

(iv) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

